package me.Opgesnoven.fm;

import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	public Main plugin;
	Logger log = Logger.getLogger("Minecraft");

	public void onEnable() {
		this.log.info("FullMaintenance has been enabled!");
		getServer().getPluginManager().registerEvents(this, this);
		getCommand("fm").setExecutor(new Commands(this));
		saveDefaultConfig();

		if (!getConfig().getBoolean("maintenance_on")) {
			String getMOTD = Bukkit.getMotd();
			getConfig().set("normal_motd", getMOTD);
		}
	}

	public void onDisable() {
		this.log.info("FullMaintenance has been disabled");
	}

	// Kijken als ze inloggen of ze permissions hebben en anders kicken, zit ook configurable message in als ze joinen!
	@EventHandler
	public void onLogin(PlayerLoginEvent e) {
		if ((getConfig().getBoolean("maintenance_on") == true) && ((!e.getPlayer().hasPermission("maintenance.bypass")))) {
			Player p = e.getPlayer();
			String n = p.getName();

			String kickmsg = getConfig().getString("maintenance_kickmessage").replaceAll("&", "�");
			e.disallow(PlayerLoginEvent.Result.KICK_OTHER, kickmsg);
			p.kickPlayer(kickmsg);

			Bukkit.broadcastMessage("�8" + n + "�7 just tried to join the server!");
		}
	}

	// Als ze joinen krijgen ze een watch item en een message die ook configurable is!
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		final Player p = (Player)e.getPlayer();

		if (getConfig().getBoolean("maintenance_on") == true) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				public void run() {
					p.sendMessage("�6Be aware that the server is currently running in MaintenanceMode! Ask an admin to toggle it using /maintenance");
				}
			}, 10L);
		}

		if(p.hasPermission("maintenance.clock")) {
			if(getConfig().getBoolean("clock_on_join")) {
				if(!p.getInventory().contains(new ItemStack(Material.WATCH))) {
					p.getInventory().addItem(new ItemStack(Material.WATCH));
				}
			}
		}
	}

	// Bij het pingen van de server een MOTD message geven en als ze ipbanned zijn ook weergeven!
	@EventHandler
	public void ServerPing(ServerListPingEvent e) {
		if (getConfig().getBoolean("maintenance_on")) {
			String IP = e.getAddress().getHostAddress();

			if(getServer().getIPBans().contains(IP)) {
				String motd = getConfig().getString("ipbanned_motd").replaceAll("&", "�");
				e.setMotd(motd);
			}

			else {
				String motd = getConfig().getString("maintenance_motd").replaceAll("&", "�");
				e.setMotd(motd);
				e.setMaxPlayers(120);
			}
		}

		else if (!getConfig().getBoolean("maintenance_on")) {
			String IP = e.getAddress().getHostAddress();

			if(getServer().getIPBans().contains(IP)) {
				String motd = getConfig().getString("ipbanned_motd").replaceAll("&", "�");
				e.setMotd(motd);
			}

			else {
				String motd = getConfig().getString("normal_motd").replaceAll("&", "�");
				e.setMotd(motd);
			}
		}
	}

	// Watch gebruiken
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		Player p = (Player) e.getPlayer();

		if(!p.hasPermission("maintenance.clock")) {

		}

		else {
			if (p.getItemInHand().getType().equals(Material.WATCH)) {
				if(getConfig().getBoolean("clock_usable")) {
					e.setCancelled(true);

					if(p.hasPermission("maintenance.toggle")) {
						p.chat("/fm");
					}

					else {

					}
				}
			}
		}
	}
}