package me.Opgesnoven.fm;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor {
	public Main plugin;

	public Commands(Main instance) {
		plugin = instance;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("fm")) {
			String perms = plugin.getConfig().getString("permissions").replaceAll("&", "�");
			final String activated = plugin.getConfig().getString("activated").replaceAll("&", "�");
			final String deactivated = plugin.getConfig().getString("deactivated").replaceAll("&", "�");
			String define_motd = plugin.getConfig().getString("define_motd").replaceAll("&", "�");
			
			if ((sender instanceof Player)) {
				if(args.length == 0) {
					Player p = (Player)sender;

					if (plugin.getConfig().getBoolean("maintenance_on")) {
						if ((!sender.hasPermission("maintenance.toggle"))) {
							p.sendMessage(perms);
							return true;
						}

						else {
							plugin.getConfig().set("maintenance_on", Boolean.valueOf(false));
							plugin.saveConfig();
							Bukkit.broadcastMessage(deactivated);
							return true;
						}
					}

					else if (!plugin.getConfig().getBoolean("maintenance_on")) {
						if ((!sender.hasPermission("maintenance.toggle"))) {
							p.sendMessage(perms);
							return true;
						}
						else {
							plugin.getConfig().set("maintenance_on", Boolean.valueOf(true));
							plugin.saveConfig();
							Bukkit.broadcastMessage(activated);
							for(Player player : Bukkit.getOnlinePlayers()) {
								if ((!player.hasPermission("maintenance.bypass"))) {
									String kickmsg = plugin.getConfig().getString("maintenance_kickmessage").replaceAll("&", "�");
									player.kickPlayer(kickmsg);
								}
							}
						}
					}
					return true;
				}

				if(args.length >= 1) {
					if(args[0].equalsIgnoreCase("motd")) {

						StringBuilder b = new StringBuilder();
						for (int i = 1; i < args.length; i++) {
							if (i != 1)
								b.append(' ');
							b.append(args[i]);
						}

						plugin.getConfig().set("maintenance_motd", b.toString());
						plugin.saveConfig();
						sender.sendMessage("�6New MOTD: �f" + b.toString().replaceAll("&", "�"));
					}

					if(args[0].equalsIgnoreCase("message")) {

						StringBuilder c = new StringBuilder();
						for (int i = 1; i < args.length; i++) {
							if (i != 1)
								c.append(' ');
							c.append(args[i]);
						}
						plugin.getConfig().set("maintenance_kickmessage", c.toString());
						plugin.saveConfig();
						sender.sendMessage("�6New kickmessage: �f" + c.toString().replaceAll("&", "�"));
					}
					
					else {
						if(isInt(args[0])) {
							int arg = Integer.parseInt(args[0]);
							int time = arg * 20;
							
							Bukkit.broadcastMessage("�6The maintenance mode of the server will be activated in: " + arg + " seconds.");
							Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								public void run() {
									if (plugin.getConfig().getBoolean("maintenance_on") == true) {
										plugin.getConfig().set("maintenance_on", Boolean.valueOf(false));
										plugin.saveConfig();
										Bukkit.broadcastMessage(deactivated);
									}

									else if (plugin.getConfig().getBoolean("maintenance_on") == false) {
										plugin.getConfig().set("maintenance_on", Boolean.valueOf(true));
										plugin.saveConfig();
										Bukkit.broadcastMessage(activated);

										for(Player player : Bukkit.getOnlinePlayers()) {
											if ((!player.hasPermission("maintenance.bypass"))) {
												String kickmsg = plugin.getConfig().getString("maintenance_kickmessage").replaceAll("&", "�");
												player.kickPlayer(kickmsg);
											}
										}
									}
								}
							}, time);
						}
						
						else {
							sender.sendMessage("�4Something went wrong while processing your command, please try it again!");
						}
					}
				}

				if(args.length == 1) {
					if(args[0].equalsIgnoreCase("motd")) {
						sender.sendMessage(define_motd);
					}
				}
			}

			// Command verstuurd vanaf de console
			else if ((sender instanceof ConsoleCommandSender)) {
				if (plugin.getConfig().getBoolean("maintenance_on") == true) {
					plugin.getConfig().set("maintenance_on", Boolean.valueOf(false));
					plugin.saveConfig();
					Bukkit.broadcastMessage(deactivated);
					return true;
				}

				else if (plugin.getConfig().getBoolean("maintenance_on") == false) {
					plugin.getConfig().set("maintenance_on", Boolean.valueOf(true));
					plugin.saveConfig();
					Bukkit.broadcastMessage(activated);

					for(Player player : Bukkit.getOnlinePlayers()) {
						if ((!player.hasPermission("maintenance.bypass"))) {
							String kickmsg = plugin.getConfig().getString("maintenance_kickmessage").replaceAll("&", "�");
							player.kickPlayer(kickmsg);
						}
					}
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isInt(String s) {
	    try {
	        Integer.parseInt(s);
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	    return true;
	}
}